/*build {
  sources = [
    "source.vsphere-iso.ubuntu-1804"
  ]

  provisioner "shell" {
    #only = ["ubuntu-1804"]
    inline = [
      "curl -sSL https://raw.githubusercontent.com/vmware/cloud-init-vmware-guestinfo/master/install.sh | sudo sh -"
    ]
  }

  provisioner "ansible" {
    playbook_file = "./playbook.yaml"
    user = "ubuntu"
    extra_arguments = ["-e -ansible_python_interpreter=/usr/bin/python3"]
  }
}
*/

build {
  sources = [
    "source.amazon-ebs.base"
  ]

  provisioner "ansible" {
    playbook_file = "./playbook.yaml"
    user = "ubuntu"
    extra_arguments = ["-e -ansible_python_interpreter=/usr/bin/python3"]
  }

  post-processor "manifest" {
      output = "manifest.json"
      strip_path = true
      custom_data = {
        my_custom_data = "example"
      }
  }  
}
